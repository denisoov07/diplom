import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import VueMask from 'v-mask'
import Vuelidate from 'vuelidate'
import locale from 'element-ui/lib/locale/lang/ru-RU'
Vue.use(ElementUI, { locale })

Vue.use(VueMask);
Vue.use(Vuelidate)
Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
