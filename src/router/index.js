import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from "../views/Login";
import Events from "../views/Events";
import Admin from "../views/Admin";
import Profile from "../views/Profile";
import Store from "../store/index"
import NotFound from "../views/NotFound";
import Moderator from "../views/Moderator";
import ro from "element-ui/src/locale/lang/ro";
import fa from "element-ui/src/locale/lang/fa";
import InfoProfile from "../views/InfoProfile";
import BadgeProfile from "../views/BadgeProfile";



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      guest: true
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      guest: true
    }
  },
  {
    path: '/events',
    name: 'events',
    component: Events,
    meta: {
      guest: true
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: 'userInfo',
        name: 'userInfo',
        component: InfoProfile,
      },
      {
        path: 'userBadge',
        name: 'userBadge',
        component: BadgeProfile,
      }
    ]
  },
  {
    path: '/admin',
    name: 'admin',
    meta: {
      requiresAuth: true,
      is_admin: true,
    },
    component: Admin,
  },
  {
    path: '/moderator',
    name: 'moderator',
    component: Moderator,
    meta: {
      requiresAuth: true,
      is_moderator: true,
    },
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound,

  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})



export default router
