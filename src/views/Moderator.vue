<template>
  <div>
    <div class="events">
      <span>Мероприятия</span>
      <el-table
        :data="events"
        style="width: 100%;"
      >
        <el-table-column
          fixed
          label="Название"
          prop="Name"
          width="300"
        >
        </el-table-column>
        <el-table-column
          label="Цель поиска"
          prop="Target"
          width="400"
        >
        </el-table-column>
        <el-table-column
          label="Подробности"
          prop="Description"
          width="900"
        >
        </el-table-column>
        <el-table-column
          label="Дата"
          prop="Date"
          width="100"
        >
        </el-table-column>
        <el-table-column
          label="Статус"
          prop="Status"
          width="300"
        >
        </el-table-column>
        <el-table-column
          label="Район"
          prop="city.Name"
          width="300"
        >
        </el-table-column>
        <el-table-column
          fixed="right"
          label="Действия"
          width="300px"
        >
        <template slot-scope="scope">
          <el-button @click="eventGetEdit(scope.$index)">Изменить</el-button>
          <el-button @click="eventDelete(scope.$index)" type="danger">Завершить</el-button>
        </template>
        </el-table-column>
      </el-table>
      <el-button @click="dialogTableVisible = true" type="primary">Добавить мероприятие</el-button>
    </div>
    <div class="event-add">
      <el-dialog :visible.sync="dialogTableVisible" width="600px">
        <div class="form">
          <el-input v-model="eventName" placeholder="Название" style="width: 350px"></el-input>
          <el-input v-model="eventTarget" placeholder="Цель поиска" style="width: 350px"></el-input>
          <el-input v-model="eventDesc" placeholder="Подробности" style="width: 350px"></el-input>
          <el-date-picker v-model="eventDate" placeholder="Дата" style="width: 350px"></el-date-picker>
          <el-select v-model="eventCity" placeholder="Район" style="width: 350px">
            <el-option
              v-for="(item, index) in cities"
              :key="index"
              :label="item.Name"
              :value="item.Id"
            >              
            </el-option>
          </el-select>
          <div>
            <el-button @click="eventPost()" type="primary">Добавить мероприятие</el-button>
          </div>
        </div>
      </el-dialog>
    </div>
    <div>
      <el-dialog :visible.sync="dialogEditTableVisible">
        <div class="form">
          <el-input v-model="eventNameEdit" placeholder="Название" style="width: 350px"></el-input>
          <el-input v-model="eventTargetEdit" placeholder="Цель поиска" style="width: 350px"></el-input>
          <el-input v-model="eventDescEdit" placeholder="Подробности" style="width: 350px"></el-input>
          <el-date-picker v-model="eventDateEdit" placeholder="Дата" style="width: 350px"></el-date-picker>
          <el-select v-model="eventCityEdit" placeholder="Район" style="width: 350px">
            <el-option
              v-for="(item, index) in cities"
              :key="index"
              :label="item.Name"
              :value="item.Id"
            >              
            </el-option>
          </el-select>
          <div>
            <el-button @click="eventPutEdit(eventIdEdit)" type="primary">Изменить</el-button>
          </div>
        </div>
      </el-dialog>
    </div>
  </div>
</template>

<script>
import axios from "axios"
import {mapGetters} from "vuex";

export default {
  name: "Moderator",
  data:()=>({
          cities: null,
          events: null,
          eventName: "",
          eventTarget: "",
          eventDesc: "",
          eventDate: "",
          eventCity: "",
          dialogTableVisible: false,
          eventIdEdit: "",
          eventNameEdit: "",
          eventTargetEdit: "",
          eventDescEdit: "",
          eventDateEdit: "",
          eventCityEdit: "",
          dialogEditTableVisible: false,
    }),
    methods:{
      // получение списка мероприятий
      eventsGet(){
        axios.get('https://dry-sea-02089.herokuapp.com/api/event')
        .then((res)=>{
          this.events = res.data;
          console.log(res)
        })
      },
      // отправка данных для добавления нового мероприятия
      eventPost(){
        axios.post('https://dry-sea-02089.herokuapp.com/api/event', {
          "Name": this.eventName,
          "Target": this.eventTarget,
          "Date": this.eventDate,
          "Description": this.eventDesc,
          "cityId": this.eventCity
        },
        {
          headers: {'Authorization': "Bearer " + this.USER_TOKEN}
        })
        .then((res)=>{
          console.log(res);
          this.eventsGet();
        })
        .catch((err) => {
          console.log(err.response.status);
          if (err.response.status === 403) {
            this.$notify.error({
              title: 'Ошибка',
              type: 'Error',
              message: 'У вас недостаточно прав',
            });
          }
        })
        .finally(()=>{
          this.eventName = '',
          this.eventTarget = '',
          this.eventDate = '',
          this.eventDesc = '',
          this.eventCity = '',
          this.dialogTableVisible = false
        })
      },
      eventDelete(index){
        axios.delete(`https://dry-sea-02089.herokuapp.com/api/event/${this.events[index].Id}`, {
          headers: {'Authorization': "Bearer " + this.USER_TOKEN}
        })
        .then((res)=>{
          console.log(res);
          this.eventsGet();
        })
        .catch((err) => {
          console.log(err.response.status);
          if (err.response.status === 403) {
            this.$notify.error({
              title: 'Ошибка',
              type: 'Error',
              message: 'У вас недостаточно прав',
            });
          }
        })
      },
      eventGetEdit(index){
        axios.get(`https://dry-sea-02089.herokuapp.com/api/event/${this.events[index].Id}`)
        .then((res)=>{
          this.eventIdEdit = res.data.Id,
          this.eventNameEdit = res.data.Name,
          this.eventTargetEdit = res.data.Target,
          this.eventDescEdit = res.data.Description,
          this.eventDateEdit = res.data.Date,
          this.eventCityEdit = res.data.cityId.Id,
          this.dialogEditTableVisible = true
        })
      },
      eventPutEdit(index){
        axios.put(`https://dry-sea-02089.herokuapp.com/api/event/${index}`, {
          "Name": this.eventNameEdit,
          "Target": this.eventTargetEdit,
          "Date": this.eventDateEdit,
          "Description": this.eventDescEdit,
          "cityId": this.eventCityEdit
        },
        {
          headers: {'Authorization': "Bearer " + this.USER_TOKEN}
        })
        .then((res)=>{
          this.eventsGet();
          console.log(res);
        })
        .catch((err) => {
          console.log(err.response.status);
          if (err.response.status === 403) {
            this.$notify.error({
              title: 'Ошибка',
              type: 'Error',
              message: 'У вас недостаточно прав',
            });
          }
        })
        .finally(() => {
          this.dialogEditTableVisible = false;
        })
      },
      citiesGet(){
        axios.get('https://dry-sea-02089.herokuapp.com/api/city')
        .then((res)=>{
          this.cities = res.data;
          console.log(res)
        })
      }
    },
    computed:{
      ...mapGetters(['USER_TOKEN'])
    },
    created(){
      // вызов методов при загрузке страницы
      this.eventsGet();
      this.citiesGet();
    }
}
</script>

<style scoped>
  .form{
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .events{
    margin-top: 50px;
  }
  .el-input{
    margin-bottom: 15px;
  }
  .el-select{
    margin-bottom: 15px;
  }
  .el-table{
    margin-bottom: 15px;
  }
</style>