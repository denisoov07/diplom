import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    preloader: false,
    events: [
      {
        name: 'День России в музее',
        description: 'Замысел организаторов – это стимулирование культурного разнообразия в автономном округе» В рамках Государственной программы «Культурное пространство».' +
            'В программе мероприятий мастер-классы, выставка экологического плаката и выставка фотографий. Таких как рисовальная площадка «Белая береза», участники которой смогут нарисовать один главных символов России – березку. ' +
            'На свежем воздухе и под руководством опытных наставников. Или выставка экологического плаката «Птичка-водичка». Плакаты созданы талантливыми югорскими студентами в разные годы.',
        status_event: 'Активно',
        place_event: 'Государственный художественный музей ул Мира, д 2',
        date_event: '21.11.2021'
      }
    ],
    user: null,
    token: null,
  },
  mutations: {
    CHANGE_PRELOADER(state, status) {
      state.preloader = status
    },
    RECORD_USER(state, data) {
      state.user = data;
      console.log(state.user)
    },
    RECORD_TOKEN(state, token) {
      state.token = token;
    },
    EXIT_USER(state, payload) {
      state.token = null;
    }

  },
  actions: {
  },
  getters: {
    USER_DATA: state => state.user,
    LIST_EVENTS: state => state.events,
    PRELOADER: state => state.preloader,
    USER_TOKEN: state => state.token,
  }
})
